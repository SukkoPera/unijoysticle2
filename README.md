# UniJoysticle 2 - Amiga Version

This is an adaptation of the [UniJoysticle 2](https://gitlab.com/ricardoquesada/unijoysticle2) so that it fits in a Commodore Amiga 500 computer. It allows you to use modern Bluetooth gamepads, e.g the Xbox One gamepad, on your Amiga 500.

**NOTE: THIS BOARD IS UNTESTED!!! USE AT YOUR RISK**

![Board](https://gitlab.com/SukkoPera/unijoysticle2/-/raw/amiga/img/render_top.png?inline=false)

## Summary
The original UniJoysticle 2 already works quite well with the Amiga 500, but you have to [use extension cables](https://gitlab.com/ricardoquesada/unijoysticle2/-/blob/master/docs/amiga_atarist.md) as it was sized to fit in a Commodore 64, which has a different spacing between the joystick ports. Therefore I set to make a new board just placing the joystick connectors at the correct distance but, while I was at it, I decided to make a few more (minor) modifications:
- Changed the DB-9 connector footprint to one that allows proper anchoring to the board.
- Removed the barrel jack: who has a barreled power supply that outputs 5V? I was going to replace it with a USB connector, but then I realized the ESP32 board already has one. In the end I just tried to simplify the power circuit, while still being able to provide extra power through USB safely: while all Amigas *should* be able to provide up to 200 mA using both joystick ports, but [the original docs recommend using an external power supply on the Amiga](https://gitlab.com/ricardoquesada/unijoysticle2/-/blob/master/docs/amiga_atarist.md), so just do it.
- Replaced the 90-degree button with a more common one and moved it to the other side of the board. I'm not even sure it is any useful on the Amiga but I think we'll find some use for it.
- Connected the PotX signal (pin 9) of port 2 to the MCU instead of PotY (pin 5) of port 1. Maybe the original design did this in order to be able to emulate a 1351 mouse but we don't need that and this will actually allow control of 2 fire buttons on each port.
- Rerouted the whole board: this was unavoidable as I had to move everything around a bit.
- Oh, I also had to make the board slightly larger: it's now 67x51 mm.

This repository only hosts the modified board. **For documentation and any information, please have a look at [the original project page](https://gitlab.com/ricardoquesada/unijoysticle2)**.

**AGAIN: THIS BOARD IS UNTESTED!!! USE AT YOUR RISK**

## Firmware
At the moment we plan to use the original firmware, which is called [Bluepad32](https://gitlab.com/ricardoquesada/bluepad32).

The flashing documentation of the original project can be found [here](https://gitlab.com/ricardoquesada/unijoysticle2/-/blob/master/docs/firmware_setup.md).

## Compatibility
The board was sized to fit on an Amiga 500. I don't know whether the port spacing is the same on other Amiga models or not, but I think I can say that if it fits another Amiga, it will work on it. Or you can just use extension cables.

## License
The UniJoysticle 2 - Amiga Version retains the license of the original project, that is the [Solderpad Hardware Licence Version 2.0](https://solderpad.org/licenses/SHL-2.0/).

## Get Help
If you need help or have questions, you can join [the official Telegram group](https://t.me/joinchat/HUHdWBC9J9JnYIrvTYfZmg).

## Credits
Original schematic, board and firmware: Ricardo Quesada (Thanks a lot for this awesome project!)

Amiga version by SukkoPera

Contributors:
- Marcelo Lorenzati: Added DualShock3 gamepad support

Many thanks to:
- Esteban/PVM and many other PVMers: Dualshock4 and general testing
- Pyrofer: Wii / Wii U beta testing and ideas
